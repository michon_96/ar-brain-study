using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundEffectHandler : MonoBehaviour
{
    [SerializeField] AudioSource sfxSource;
    [SerializeField] AudioSource bgSource;

    public void PlaySFX(bool turnOn)
    {
        if (turnOn)
        {
            sfxSource.Play();
        }
        else
        {
            sfxSource.Stop();
        }
    }

    public void PlayBG(bool turnOn)
    {
        if (turnOn)
        {
            bgSource.Play();
        }
        else
        {
            bgSource.Stop();
        }
    }

}