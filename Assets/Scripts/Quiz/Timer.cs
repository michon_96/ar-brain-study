using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public static Timer instance;
    public static Timer getInstance()
    {
        return instance;
    }

    [Header("UI Objects")]
    [SerializeField] Image timeBar = null;
    [Tooltip("Time it takes for user to answer the question")]
    [SerializeField] float activityTimeRange;
    [Tooltip("Time Running during the waiting for next Question")]
    [SerializeField] bool isPaused = false;
    [SerializeField] float deltaTime = 0;

    void Awake()
    {
        DontDestroyOnLoad(this);
        if (instance == null) { instance = this; }
    }
    void Start()
    {
        deltaTime = activityTimeRange;
        //isPaused = true;
    }

    void FixedUpdate()
    {
        if (!isPaused && deltaTime > 0)
        {
            deltaTime -= Time.fixedDeltaTime;
        }
        timeBar.fillAmount = (float)deltaTime / (float)activityTimeRange;
        if (timeBar.fillAmount <= 0)
        {
            QuizManager.sharedInstance.showEndResult();
        }
    }

    public void reset()
    {
        isPaused = false;
        deltaTime = activityTimeRange;
    }

    public void pause(bool pauseTime)
    {
        isPaused = pauseTime;
    }

}