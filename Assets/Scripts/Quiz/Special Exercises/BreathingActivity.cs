using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine;

public class BreathingActivity : MonoBehaviour
{
    [Header("UI Objects")]
    [SerializeField] private Button startButton;
    [SerializeField] private TextMeshProUGUI instructionText;
    [SerializeField] Image timeBar = null;

    [Header("Breathing Variables")]
    [SerializeField] private int inhaleSeconds = 4;
    [SerializeField] private int holdSeconds = 7;
    [SerializeField] private int exhaleSeconds = 8;
    [SerializeField] private string[] breathingInstructions = { "Inhale", "hold", "exhale" };
    [SerializeField] private string endMessage = "Good job! Kudos! Now repeat this until you master it.";
    [SerializeField] int numberOfCycles = 2;
    [SerializeField] int currentCycle = 0;

    bool isPaused;
    bool isForward;
    bool isWaiting;
    [SerializeField] float deltaTime = 0;
    [SerializeField] float currentMaxTimer = 0;

    void FixedUpdate()
    {
        if (!isForward && !isPaused && deltaTime > 0)
        {
            deltaTime -= Time.fixedDeltaTime;
        }
        if (isForward && !isPaused)
        {
            deltaTime += Time.fixedDeltaTime;
        }
        runVisualTimer();
    }

    public void runBreathingCycle()
    {
        StartCoroutine(runTimer());
    }

    public IEnumerator runTimer()
    {
        if (isWaiting)
            yield break;
        for (int i = 0; i < numberOfCycles; i++)
        {
            do
            {
                int timeToWait = getMaxTimer();
                isWaiting = true;
                deltaTime = timeToWait;
                setTimerDirection();
                instructionText.SetText(breathingInstructions[currentCycle]);
                yield return new WaitForSeconds(timeToWait);
                isPaused = false;
                currentCycle++;
            } while (currentCycle < 3);
            currentCycle = 0;
        }
        QuizManager.sharedInstance.showEndResult(endMessage);
        startButton.gameObject.SetActive(true);
        instructionText.gameObject.SetActive(false);
    }

    private void runVisualTimer()
    {
        timeBar.fillAmount = (float)deltaTime / (float)getMaxTimer();
    }

    private int getMaxTimer()
    {
        switch (currentCycle)
        {
            case 0:
                return inhaleSeconds;
            case 1:
                return holdSeconds;
            case 2:
                return exhaleSeconds;
            default:
                return -1;
        }
    }

    private void setTimerDirection()
    {
        switch (currentCycle)
        {
            case 0:
                isForward = true;
                deltaTime = 0;
                break;
            case 1:
                isPaused = true;
                break;
            case 2:
                isForward = false;
                break;
            default:
                break;
        }
    }
}