using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SoundMatchActivity : MonoBehaviour
{
    [SerializeField] private AudioSource source;
    [SerializeField] private Text questionIndexIndicator;
    [SerializeField] private bool isPlaying = false;
    [SerializeField] private ImageChoice[] imageChoices;
    [SerializeField] private Stack<int> undoHistory;

    void Start()
    {
        source = this.GetComponent<AudioSource>();
        undoHistory = new Stack<int>();
    }

    public void undoLastChoice()
    {
        try
        {
            imageChoices[undoHistory.Pop()].unSelectChoice();
        }
        catch (Exception e)
        {

        }
    }

    public bool isAllSelected()
    {
        foreach (var choice in imageChoices)
        {
            if (!choice.getSelected())
            {
                //Debug.Log($"choice not selected{choice.name}");
                return false;
            }
        }
        return true;
    }

    public void updateUI()
    {
        questionIndexIndicator.text = $"Question {QuizManager.sharedInstance.getCurrentQuestionIndex() + 1}";
    }

    public void reset()
    {
        foreach (var choice in imageChoices)
        {
            choice.unSelectChoice();
        }
    }

    public void setLastIndex(int ind)
    {
        undoHistory.Push(ind);
    }

    public void playClip()
    {
        if (isAllSelected())
        {
            stopPlaying();
            return;
        }

        if (source.isPlaying)
            stopPlaying();

        source.clip = QuizManager.getInstance().getAuditoryQuestion();
        source.loop = true;
        source.Play();
        isPlaying = true;
    }

    public void stopPlaying()
    {
        source.Stop();
        source.loop = false;
        source.clip = null;
        isPlaying = false;
    }

}