using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public static ScoreManager instance;
    public static ScoreManager getInstance()
    {
        return instance;
    }

    [Header("UI Objects")]
    [SerializeField] Text uiScore;
    [SerializeField] Text uiEndResultScore;

    [Header("Score Params")]
    [SerializeField] int totalScore;
    [SerializeField] int correctAns;
    [SerializeField] int wrongAns;
    [SerializeField] bool isPreviousAnsCorrect = false;

    public int WrongAns { get => wrongAns; set => wrongAns = value; }
    public int CorrectAns { get => correctAns; set => correctAns = value; }

    void Awake()
    {
        DontDestroyOnLoad(this);
        if (instance == null) { instance = this; }
    }

    void Start()
    {
        reset();
    }

    public void setTotalScore(int score)
    {
        totalScore = score;
    }

    public void setEndMessage(string endMessage)
    {
        uiEndResultScore.text = endMessage;
    }

    public void reset()
    {
        wrongAns = 0;
        correctAns = 0;
        totalScore = QuizManager.getInstance().getTotalQuestions();
        updateScore();
    }
    public void addScore(bool isAnswerCorrect)
    {
        if (isAnswerCorrect)
        { 
            correctAns++;
        }
        else
        {
            if (correctAns < 0)
                correctAns = 0;
        }
        updateScore();
    }

    void updateScore()
    {
        float formula = ((float)correctAns / (float)totalScore);
        float totalCorrectPercentage = (formula) * 100;
        uiScore.text = $"{totalCorrectPercentage}%";
    }

    public void returnPreviousAnswer()
    {
        if (isPreviousAnsCorrect)
            correctAns--;
        else
            wrongAns--;
    }

    public void finalizeScore()
    {
        //uiEndResultScore.text = $"{correctAns}/{totalScore}";
        uiEndResultScore.text = $"Activity Completed";
    }
    
}