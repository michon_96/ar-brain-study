﻿
[System.Serializable]
public class Exercise
{
    public string name;
    public string instructions;
    public bool isScored;
    public Question[] questions;
}
