using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class AuditoryQuestion : Question
{
    public AudioClip clip;
}