using UnityEngine;
using UnityEngine.UI;


public class ImageChoice : MonoBehaviour
{
    [SerializeField] private Color imageSelectedColor = Color.blue;
    [SerializeField] private Color imageUnSelectedColor = Color.white;
    [SerializeField] private bool isSelected = false;
    private Image objImg;
    private Button bttn;

    void Start()
    {
        objImg = this.GetComponent<Image>();
        bttn = this.GetComponent<Button>();
    }

    public void selectChoice()
    {
        bttn.enabled = false;
        isSelected = true;
        objImg.color = imageSelectedColor;
    }
    public void unSelectChoice()
    {
        isSelected = false;
        bttn.enabled = true;
        objImg.color = imageUnSelectedColor;
    }

    public bool getSelected()
    {
        return isSelected;
    }

}
