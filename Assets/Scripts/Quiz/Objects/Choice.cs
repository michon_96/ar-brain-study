using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public struct Choice
{
    public string answer;
    public bool isCorrect;
}
