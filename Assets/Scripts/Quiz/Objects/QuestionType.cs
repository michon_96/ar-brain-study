using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum QuestionType 
{
    //Add as needed
    Identification,
    Enumeration,
    TrueOrFalse,
    Match,
    MultipleChoice
}
