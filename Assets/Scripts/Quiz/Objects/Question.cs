using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Question
{
    public QuestionType type = QuestionType.MultipleChoice;
    public string question;
    public Choice[] choices;

    public Question()
    {
        choices = new Choice[1];
    }
}
