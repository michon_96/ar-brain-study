using Michsky.UI.ModernUIPack;
using System;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(VisualProgress))]
public class QuizManager : MonoBehaviour
{
    public static QuizManager sharedInstance;
    [SerializeField] private SoundMatchActivity specialMatchActivity;
    private int currentQuestionIndex = -1;
    [SerializeField] private Question currentQuestion;

    [Header("Prefabs")]
    [SerializeField] private GameObject choiceButton;

    [Header("UI Objects")]
    [SerializeField] private GameObject endResultObj;
    [SerializeField] private GameObject multipleChoiceGroupObj;
    [SerializeField] private GameObject choicesObjParent;
    [SerializeField] private TextMeshProUGUI questionUIText;
    [SerializeField] private TextMeshProUGUI activityTitle;
    [SerializeField] private TextMeshProUGUI instructionsUIText;
    /*[SerializeField]*/ private GameObject inputTxtObjParent;
    [SerializeField] private GameObject[] specialQuestionsGameObjects;

    [Header("Background")]
    Sprite m_originalBGImage;
    [SerializeField] private Image backgroundImageUI;
    [SerializeField] private Sprite[] backgroundImages;

    [Header("Questions and Answers")]
    [SerializeField] private int selectedActivity = -1;
    /*[SerializeField]*/ private Question[] questions = new Question[0];
    [SerializeField] private AuditoryQuestion[] auditoryQuestions;
    [SerializeField] private Exercise[] exercise;

    [SerializeField] private bool isSpecialExercise;

    private ScoreManager scoreManager;
    //private Timer timerManager;
    private VisualProgress visualProgress;


    public static QuizManager getInstance()
    {
        return sharedInstance;
    }
    private void Awake()
    {
        if (sharedInstance != null && sharedInstance != this)
        {
            Destroy(this.gameObject);
        }

        if (sharedInstance == null) sharedInstance = this;
        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        scoreManager = ScoreManager.getInstance();
        m_originalBGImage = backgroundImageUI.sprite;  //timerManager = Timer.getInstance();
        visualProgress = gameObject.GetComponent<VisualProgress>();
        currentQuestionIndex = -1;


        foreach (GameObject go in specialQuestionsGameObjects)
        {
            go.SetActive(false);
        }

        // Shuffle auditory questions order
        for (int i = 0; i < auditoryQuestions.Length - 1; i++)
        {
            int rnd = UnityEngine.Random.Range(i, auditoryQuestions.Length);
            var tempAQ = auditoryQuestions[rnd];
            auditoryQuestions[rnd] = auditoryQuestions[i];
            auditoryQuestions[i] = tempAQ;
        }

        
        selectExercise();
        reset();
        setInstructions();
    }

    private void setInstructions()
    {
        activityTitle.text = $"Activity {selectedActivity + 1}: \n{exercise[selectedActivity].name}";
        try
        {
            if (exercise[selectedActivity].instructions != "" || exercise[selectedActivity].instructions != null)
                instructionsUIText.text = exercise[selectedActivity].instructions;
        }
        catch (NullReferenceException e)
        {
            Console.WriteLine(e);
            instructionsUIText.text = "Follow the instructions and choose the answer you think is correct.";
            throw;
        }
    }

    private bool isLRHemisphere()
    {
        string partSelected = PartInspectorManager.getInstance().getSelectedPart().ToLower();
        if (partSelected.Contains("Left") || partSelected.Contains("Right") || partSelected.Contains("Hemisphere"))
        {
            return true;
        }
        return false;
    }

    private void selectExercise()
    {
        for (int i = 0; i < exercise.Length; i++)
        {
            string exerciseName = exercise[i].name.ToLower();
            string partSelected = PartInspectorManager.getInstance().getSelectedPart().ToLower();
            if (isLRHemisphere())
            {
                selectedActivity = 5;
                break;
            }
            if (exerciseName.Equals(partSelected))
            {
                Debug.Log($"Using Exercise {i} = {exercise[i].name}");
                selectedActivity = i;
                break;
            }
        }
        questions = exercise[selectedActivity].questions;
        SelectBackgroundImage(selectedActivity);
        showSpecialExerciseUI();
    }

    void SelectBackgroundImage(int p_bgIndex)
    {
        try
        {
            backgroundImageUI.sprite = backgroundImages[p_bgIndex];
        }
        catch (IndexOutOfRangeException e)
        {
            if (isLRHemisphere())
            {
                backgroundImageUI.sprite = backgroundImages[backgroundImages.Length - 1];
            }
            else
            {
                Debug.Log("BG Image not found");
                backgroundImageUI.sprite = m_originalBGImage;
            }
        }
    }

    private void showSpecialExerciseUI()
    {
        switch (selectedActivity)
        {
            case 0:
                multipleChoiceGroupObj.SetActive(false);
                specialQuestionsGameObjects[0].SetActive(true);
                break;
            case 1:
                multipleChoiceGroupObj.SetActive(false);
                specialQuestionsGameObjects[1].SetActive(true);
                questions = auditoryQuestions;
                //  timerManager.enabled = false;
                break;
            default:
                multipleChoiceGroupObj.SetActive(true);
                if (specialQuestionsGameObjects[0].activeSelf || specialQuestionsGameObjects[1].activeSelf)
                {
                    specialQuestionsGameObjects[0].SetActive(false);
                    specialQuestionsGameObjects[1].SetActive(false);
                }
                break;
        }
    }

    private void isExerciseScored()
    {
        if (exercise[selectedActivity].isScored)
        {
        }
    }

    private void populateChoices(QuestionType questionType)
    {
        inputTxtObjParent.SetActive(false);
        choicesObjParent.SetActive(false);

        switch (questionType)
        {
            case QuestionType.Enumeration:
                inputTxtObjParent.SetActive(true);
                break;

            case QuestionType.MultipleChoice:
                choicesObjParent.SetActive(true);
                GenerateChoiceButtons(questions[currentQuestionIndex].choices, false);
                break;
        }
    }

    public void moveQuestion(bool nextQuestion)
    {
        moveQuestionIndex(nextQuestion);
        if (checkEndOfExercise()) return;
        questionUIText.text = $"{(questions.Length > 1 ? $"{(currentQuestionIndex + 1).ToString()}. " :"")}{currentQuestion.question}";
        visualProgress.setSelected(currentQuestionIndex);
        populateChoices();
    }

    public void moveAuditoryQuestion(bool nextQuestion)
    {
        bool isAllSelected = specialMatchActivity.isAllSelected();
        moveQuestionIndex(nextQuestion, isAllSelected);
    }

    private void moveQuestionIndex(bool nextQuestion)
    {
        if (currentQuestionIndex >= questions.Length)
            return;

        if (nextQuestion && currentQuestionIndex < questions.Length)
            currentQuestionIndex++;
        else if (currentQuestionIndex <= 0)
            currentQuestionIndex--;

        if (checkEndOfExercise())
        {
            showEndResult();
            return;
        }

        currentQuestion = questions[currentQuestionIndex];
    }

    private void moveQuestionIndex(bool nextQuestion, bool keepLooping)
    {
        if (keepLooping)
        {
            showEndResult();
            return;
        }

        if (nextQuestion && currentQuestionIndex < questions.Length)
            currentQuestionIndex++;
        else if (!nextQuestion)
            currentQuestionIndex--;

        if (currentQuestionIndex >= questions.Length)
            currentQuestionIndex = 0;
        if (currentQuestionIndex <= -1)
            currentQuestionIndex = questions.Length - 1;

        currentQuestion = questions[currentQuestionIndex];
    }

    public int getTotalQuestions()
    {
        return questions.Length;
    }

    public int getCurrentQuestionIndex()
    {
        return currentQuestionIndex;
    }

    public void showEndResult()
    {
        specialMatchActivity.stopPlaying();
        scoreManager.setTotalScore(questions.Length);
        //timerManager.pause(true);
        endResultObj.SetActive(true);
        scoreManager.finalizeScore();
    }

    public void showEndResult(string endMessage)
    {
        //timerManager.pause(true);
        scoreManager.setEndMessage(endMessage);
        endResultObj.SetActive(true);
    }

    private void populateChoices()
    {
        clearChoices();
        if (questions[currentQuestionIndex].choices.Length > 0)
        {
            choicesObjParent.SetActive(true);
            GenerateChoiceButtons(questions[currentQuestionIndex].choices);
        }
        else
        {
            inputTxtObjParent.SetActive(true);
        }
    }

    private void clearChoices()
    {
        foreach (Transform child in choicesObjParent.transform) Destroy(child.gameObject);
    }

    public void reset()
    {
        if ((exercise[selectedActivity].name.Equals("Temporal Lobe") || selectedActivity == 1) && currentQuestionIndex >= 1)
        {
            specialMatchActivity.reset();
        }
        currentQuestionIndex = -1;
        moveQuestion(true);
        // timerManager.reset();
        scoreManager.reset();

    }

    private void randomizeQuestions()
    {
        Question[] newList = new Question[questions.Length];
        int index = 0;
        do
        {
            System.Random rand = new System.Random();
            var randomIndex = rand.Next(0, questions.Length);
            if (newList.Contains(questions[randomIndex]))
            {
                newList[index] = questions[randomIndex];
                index++;
            }
        } while (newList.Length != questions.Length);

        questions = newList;
    }

    private void checkAnswer()
    {
        switch (currentQuestion.type)
        {
            default:
                // checkMultipleChoiceAnswer();
                break;
        }
    }

    private void GenerateChoiceButtons(Choice[] items, bool useButtonManager = true)
    {
        if ((items.Length % 2) == 0)
        {
            //even
            choicesObjParent.gameObject.GetComponent<Beardy.GridLayoutGroup>().startAxis = GridLayoutGroup.Axis.Vertical;
        }
        else
        {
            //odd
            choicesObjParent.gameObject.GetComponent<Beardy.GridLayoutGroup>().startAxis = GridLayoutGroup.Axis.Horizontal;
        }


        int index = 0;
        foreach(Choice item in items)
        {
            GameObject go = Instantiate<GameObject>(choiceButton, choicesObjParent.transform);
            RectTransformExtension.SetDefaultScale(go.GetComponent<RectTransform>());

            if (useButtonManager)
            {
                go.GetComponent<ButtonManagerBasic>().buttonText = item.answer;
                
            } 
            else
            {
                go.GetComponentInChildren<Text>().text = item.answer;
            }

            go.name = index.ToString();
            index += 1;
        }
    }

    public void checkMultipleChoiceAnswer(GameObject obj)
    {
        var userChoiceInput = int.Parse(obj.name);
        var selected = currentQuestion.choices[userChoiceInput];
        scoreManager.addScore(selected.isCorrect);
    }

    public void checkAuditoryAnswer(GameObject answer)
    {
        bool isCorrect = currentQuestion.choices[0].answer.ToLower().Contains(answer.name.ToLower());
        scoreManager.addScore(isCorrect);
    }

    public void checkIdentificationAnswer()
    {
        var inputAnswer = inputTxtObjParent.GetComponentInChildren<Text>().text;
        var isUserCorrect = inputAnswer.Equals(currentQuestion.choices[0].answer);
        scoreManager.addScore(isUserCorrect);
    }

    private bool checkEndOfExercise()
    {
        if (currentQuestionIndex >= questions.Length)
        {
            showEndResult();
            return true;
        }

        return false;
    }

    public bool isExerciseUnique()
    {
        return isSpecialExercise;
    }

    public AudioClip getAuditoryQuestion()
    {
        return (currentQuestion as AuditoryQuestion).clip;
    }

}