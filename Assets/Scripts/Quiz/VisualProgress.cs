using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class VisualProgress : MonoBehaviour
{
    [Header("Prefab")]
    [SerializeField] GameObject uiIndicator;
    [SerializeField] Color selectedColor;
    [SerializeField] Color unselectedColor;

    [Header("Required")]
    [SerializeField] GameObject progressParent;

    [Header("Selection")]
    [SerializeField] List<GameObject> indicators;

    void Start()
    {
        poolIndicators();
    }

    void poolIndicators()
    {
        for (int objIndex = 0; objIndex < QuizManager.sharedInstance.getTotalQuestions(); objIndex++)
        {
            GameObject circle = Instantiate(uiIndicator);
            if (objIndex == 0)
            {
                circle.GetComponent<Image>().color = selectedColor;
            }
            else
            {
                circle.GetComponent<Image>().color = unselectedColor;
            }
            circle.transform.parent = progressParent.transform;
            indicators.Add(circle);
        }
    }

    public void setSelected(int selected)
    {
        for (int index = 0; index < indicators.Count; index++)
        {
            if (index == selected)
            {
                indicators[index].GetComponent<Image>().color = selectedColor;
            }
            else
            {
                indicators[index].GetComponent<Image>().color = unselectedColor;
            }
        }
    }

}