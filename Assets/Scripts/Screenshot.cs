using System;
using System.Collections;
using TMPro;
using System.IO;
using Michsky.UI.ModernUIPack;
using UnityEngine;
using UnityEngine.Android;

public class Screenshot : MonoBehaviour
{
    private string folderPath = "/storage/emulated/0";
    [SerializeField] NotificationManager notificationManager;
    [SerializeField] float screenshotAnimDelay = 1.5f;
    [SerializeField] Animator anim;

    void Start()
    {
        anim = this.GetComponent<Animator>();
        requestPermission();
    }

    private void requestPermission()
    {
        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageRead))
        {
            Permission.RequestUserPermission(Permission.ExternalStorageRead);
        }
        if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
        {
            Permission.RequestUserPermission(Permission.ExternalStorageWrite);
        }
    }

    public void takeScreenshot()
    {
        StartCoroutine(TakeScreenShotEnumerator());
    }

    public IEnumerator TakeScreenShotEnumerator()
    {
        var screenshotName =
            "Brain_AR_Screenshot_" +
            System.DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") +
            ".png";
        ScreenCapture.CaptureScreenshot(screenshotName);
        yield return new WaitForSeconds(screenshotAnimDelay);
        checkScreenShot(screenshotName);
        anim.Play("Screenshot");
        ShowScreenshotSuccessful();
    }

    private void checkScreenShot(string fileName)
    {
        string origin = Path.Combine(Application.persistentDataPath, fileName);
        Debug.Log($"origin {origin}");
        Debug.Log($"File Exists {File.Exists(origin)}");
    }

    public void ShowScreenshotSuccessful()
    {
        notificationManager.title = $"Screenshot Saved";
        notificationManager.description = $"Saved in {folderPath}";
        notificationManager.OpenNotification();
    }

    public void ShowScreenshotUnsuccessful()
    {
        notificationManager.titleObj.text = $"Screenshot Unsuccessful";
        notificationManager.descriptionObj.text = $"Please enable storage permission.";
        notificationManager.OpenNotification();
    }

}