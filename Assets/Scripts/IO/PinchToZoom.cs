using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PinchToZoom : MonoBehaviour
{
    Touch touch1, touch2;
    [SerializeField] float scaleSensitivity = 0.5f;
    [SerializeField] float maxScaleDistance = 3f;
    [SerializeField] float minScaleDistance = 1f;

    [SerializeField] GameObject brainToScale;
    private Vector3 objectOriginScale;
    private Vector3 objectDefaultScale;
    private float distance1;

    void Start()
    {
        objectDefaultScale = brainToScale.transform.localScale;
    }

    void LateUpdate()
    {
        scaleInput();
    }

    private void scaleInput()
    {
        if (Input.touchCount > 0)
        {
            touch1 = Input.GetTouch(0);

            if (Input.touchCount == 2)
            {
                touch2 = Input.GetTouch(1);

                if (touch2.phase == TouchPhase.Began)
                {
                    objectOriginScale = brainToScale.transform.localScale;
                    distance1 = calculateDistance(touch1.position, touch2.position);
                }

                if (touch2.phase == TouchPhase.Moved)
                {
                    scale(touch1.position, touch2.position);
                }
                if (touch2.phase == TouchPhase.Ended)
                {
                    //brainToScale.transform.localScale = objectOriginScale;
                }
            }
        }
    }
    private float calculateDistance(Vector2 finger1, Vector2 finger2)
    {
        float distance = 0;
        float distanceX = Mathf.Pow(Mathf.Abs(finger2.x - finger1.x) / 10, 2);
        float distanceY = Mathf.Pow(Mathf.Abs(finger2.y - finger1.y) / 10, 2);

        return distance = Mathf.Sqrt(distanceX + distanceY);
    }

    public void scale(Vector2 finger1, Vector2 finger2)
    {
        float distance2 = calculateDistance(finger1, finger2);
        float scale = distance2 - distance1;
        float scaleXY = objectOriginScale.x + scale * scaleSensitivity;

        if (scaleXY >= maxScaleDistance) { scaleXY = maxScaleDistance; }
        else if (scaleXY <= minScaleDistance) { scaleXY = minScaleDistance; }
        brainToScale.transform.localScale = new Vector3(scaleXY, scaleXY, scaleXY);
    }
}