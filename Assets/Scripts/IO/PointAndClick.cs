using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointAndClick : MonoBehaviour
{
    Camera camera;
    VoiceOverManager voiceOverManager;
    PartInspectorManager partManager;
    [SerializeField] GameObject uiObject;
    [SerializeField] SoundEffectHandler sfxManager;
    [SerializeField] RotateToView rotateToView;
    [SerializeField] GameObject m_brainPref;
    [SerializeField] Transform m_brainHolder;
    GameObject m_brain;
    Ray ray;
    RaycastHit hit;

    void Start()
    {
        camera = Camera.main;
        voiceOverManager = VoiceOverManager.getInstance();
        partManager = PartInspectorManager.getInstance();
    }

    // Update is called once per frame
    void Update()
    {

#if PLATFORM_ANDROID
        if (Input.touchCount > 0)
        {
            for (int i = 0; i < Input.touchCount; i++)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Began)
                {
                    Vector3 pos = Input.GetTouch(i).position;
                    Vector3 p = Camera.main.ScreenToWorldPoint(new Vector3(pos.x, pos.y, Camera.main.nearClipPlane));
                    ray = camera.ScreenPointToRay(Input.GetTouch(0).position);
                    selectTarget();
                }
            }
        }
#endif

#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            ray = camera.ScreenPointToRay(Input.mousePosition);
            selectTarget();
        }
#endif

    }

    void selectTarget()
    {
        if (Physics.Raycast(ray, out hit))
        {
            try
            {
                Transform objectHit = hit.transform;
                if (uiObject.activeInHierarchy)
                    return;

                int partIndex = int.Parse(objectHit.gameObject.name) - 1;
                partManager.updateUI(partIndex);
                sfxManager.PlaySFX(true);
                sfxManager.PlayBG(false);

                uiObject.SetActive(!uiObject.active);


                StopCoroutine("ShowBrainInfo");
                StartCoroutine(ShowBrainInfo(partIndex));
            }
            catch (UnityException e)
            {
                /*DebugUI.instance.addText(e.Message);*/
            }
        }
    }

    IEnumerator ShowBrainInfo(int p_index)
    {
        m_brain = Instantiate(m_brainPref, m_brainHolder);
        yield return null;
        rotateToView.transform.rotation = Quaternion.Euler(Vector3.zero);
        rotateToView.activateRotation = false;
        voiceOverManager.ResetMuteButton();
        voiceOverManager.stopVoiceOver();
        voiceOverManager.EnableMuteButton(false);
        m_brain.GetComponent<Animator>().SetTrigger(p_index.ToString());
        yield return new WaitForSeconds(3);
        voiceOverManager.EnableMuteButton(true);
        rotateToView.activateRotation = true;
        voiceOverManager.playAudio(p_index);
    }

    public void HideBrainInfo()
    {
        StopAllCoroutines();
        if (m_brain != null)
            Destroy(m_brain);
        voiceOverManager.stopVoiceOver();
        //StartCoroutine(HideBrain());
    }

    //IEnumerator HideBrain()
    //{
    //    rotateToView.transform.rotation = Quaternion.Euler(Vector3.zero);
    //    rotateToView.activateRotation = false;
    //    audioManager.stopVoiceOver();
    //    modelBrain.GetComponent<Animator>().SetTrigger("reset");
    //    yield return null;
    //    modelBrain.SetActive(false);
    //}

}