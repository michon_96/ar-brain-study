using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragToRotate : MonoBehaviour
{
    public float mousePosX;
    public float mousePosY;
    public float rotationSensitivity = 1f;

    public Transform objectTransform;
    public LayerMask moveableObject;
    private Vector3 dragOrigin;
    private Quaternion originalRotation;
    private Quaternion lastRotation;
    private Vector3 lastRotationEuler;
    float prevDistance = 0;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            originalRotation = objectTransform.rotation;
            dragOrigin = Input.mousePosition;
        }
        if (Input.GetMouseButton(0))
        {
            Vector3 touchPosition = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragOrigin);
            float distance = calculateDistance(Input.mousePosition, dragOrigin);
            //Debug.Log(distance);
            if (distance != prevDistance)
            {
                prevDistance = distance;
                objectTransform.Rotate(new Vector3(touchPosition.y * rotationSensitivity, -touchPosition.x * rotationSensitivity, 0), Space.World);

            }

            //transform.localRotation *= Quaternion.AngleAxis(distance, touchPosition);
        }

        if (Input.GetMouseButtonUp(0))
        {
            prevDistance = 0;
        }
    }

    private float calculateDistance(Vector2 finger1, Vector2 finger2)
    {
        float distance = 0;
        float distanceX = Mathf.Pow(Mathf.Abs(finger2.x - finger1.x) / 10, 2);
        float distanceY = Mathf.Pow(Mathf.Abs(finger2.y - finger1.y) / 10, 2);

        return distance = Mathf.Sqrt(distanceX + distanceY);
    }

    void rotateEuler(Vector3 touchPosition)
    {
        /*      objectTransform.rotation = new Quaternion(
                                           originalRotation.x + (pos.y * rotationSensitivity),
                                            originalRotation.y + (pos.x * -rotationSensitivity),
                                             originalRotation.z, rotationSensitivity);*/
        /*        if (touchPosition.x <= 0 || touchPosition.y <= 0)
                    return;
    */

        Debug.Log($"touchPosition = {touchPosition}");
        Debug.Log($"brainHolder rotation/original Rotation = {objectTransform.rotation}");

        Quaternion calculatedRotation = new Quaternion(
            rotationSensitivity * touchPosition.y,
            -rotationSensitivity * touchPosition.x,
            0,
            1);

        objectTransform.rotation = Quaternion.Lerp(
        originalRotation,
        calculatedRotation,
        rotationSensitivity);
        Debug.Log($"Euler Calc = {calculatedRotation}");
    }
}