using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneHandler : MonoBehaviour
{

    //[SerializeField] private GameObject loadingBrainPrefab;
    [SerializeField] private float loadingTime;
    public void LoadScene(string sceneName)
    {

        StartCoroutine(ShowLoadingScreen(sceneName));
    }

    public void LoadScene(int sceneIndex)
    {
        StartCoroutine(showLoadingScreen(sceneIndex));
    }

    public IEnumerator showLoadingScreen(int index)
    {
        //loadingBrainPrefab.SetActive(true);
        yield return new WaitForSeconds(loadingTime);
        SceneManager.LoadSceneAsync(index);
    }

    public IEnumerator ShowLoadingScreen(string p_sceneName)
    {
        //loadingBrainPrefab.SetActive(true);
        yield return new WaitForSeconds(loadingTime);
        SceneManager.LoadSceneAsync(p_sceneName);
    }
}