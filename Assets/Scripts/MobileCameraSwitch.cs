using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class MobileCameraSwitch : MonoBehaviour
{
    public ARCameraManager arCameraManager;
    public bool isFrontCam = false;

    void Start()
    {
        arCameraManager = this.GetComponent<ARCameraManager>();
    }

    public void switchCamera()
    {
        if (isFrontCam)
        {
            arCameraManager.requestedFacingDirection = CameraFacingDirection.World;
            isFrontCam = false;
        }
        else
        {
            arCameraManager.requestedFacingDirection = CameraFacingDirection.User;
            isFrontCam = true;
        }
    }
}