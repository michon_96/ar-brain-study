using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;
using UnityEngine;

public class VoiceOverManager : MonoBehaviour
{
    private static VoiceOverManager instance;
    public static VoiceOverManager getInstance()
    {
        return instance;
    }

    [SerializeField] private AudioSource audioSrc;
    [SerializeField] GameObject muteUIO;
    [SerializeField] private Text muteText;

    [SerializeField] private Sprite spriteMute;
    [SerializeField] private Sprite spriteUnmute;

    [SerializeField] List<AudioClip> clipsFound;
    [SerializeField] List<AudioClip> voiceOverClips;

    private Button ctaButton;
    private Image ctaImage;
    private Text ctaText;

    void Awake()
    {
        DontDestroyOnLoad(this);
        if (instance == null) { instance = this; }
        else
        {
            Destroy(this);
        }
    }

    void Start()
    {
        audioSrc = this.GetComponent<AudioSource>();
        ctaButton = muteUIO.GetComponent<Button>();
        ctaImage = muteUIO.GetComponent<Image>();
        ctaText = muteUIO.GetComponentInChildren<Text>();

        PauseAudio();
    }

    public void playAudio(string partName)
    {
        foreach (var clip in clipsFound)
        {
            if (clip.name.Contains(partName))
            {
                audioSrc.clip = clip;
                break;
            }
        }
        audioSrc.Play();
    }

    public void playAudio(int index)
    {
        audioSrc.clip = voiceOverClips[index];
        audioSrc.Play();
        ctaImage.sprite = spriteUnmute;
        ctaText.text = "Mute";
    }

    public void PauseAudio()
    {
        if (audioSrc.isPlaying)
        {
            audioSrc.Pause();
            ShowMuteButton();
        }
        else
        {
            audioSrc.Play();
            ShowPlayButton();
        }
    }

    public void stopVoiceOver()
    {
        audioSrc.Stop();
    }

    public void EnableMuteButton(bool enable)
    {
        ctaButton.interactable = enable;
        ctaText.color = enable ? Color.white : new Color(200, 200, 200);
    }

    private void ShowMuteButton()
    {
        ctaText.text = "Play";
        ctaImage.sprite = spriteMute;
    }

    private void ShowPlayButton()
    {
        ctaText.text = "Mute";
        ctaImage.sprite = spriteUnmute;
    }

    public void ResetMuteButton()
    {
        audioSrc.Pause();
        ShowPlayButton();
    }

    private AudioClip findObjectAsset(string fileName)
    {
        string Path = $"/Resources/Audio/The Brain -{fileName}.mp3";

        Debug.Log("path :" + Path);
        try
        {
            var rvdClip = Resources.Load(Path);
            return (AudioClip)rvdClip;
        }
        catch (UnityException e)
        {
            Debug.Log(e.Message);
            return null;
        }
    }

    public AudioClip playAudioFromResource(string partName)
    {
        string path = $"Assets/Resources/Audio Files";
        string[] folderNames = Directory.GetDirectories(path);
        string folderFound = "";
        string[] insideFiles;
        AudioClip voiceOverFile = clipsFound[0];

        try
        {

            foreach (var folderName in folderNames)
            {
                string full = folderName.ToLower();
                string substring = folderName.Substring(28).ToLower();
                if (full.Contains(partName.ToLower()) || substring.Contains(partName.ToLower()))
                {
                    folderFound = folderName;
                }
            }
            Debug.Log($"Using folder {folderFound}");

            insideFiles = Directory.GetFiles(folderFound);
            foreach (var fileName in insideFiles)
            {
                if (fileName.ToLower().Contains(".wav") && fileName.ToLower().Contains(name.ToLower()) && !fileName.Contains(".meta"))
                {
                    int fileNameIndex = fileName.IndexOf('\"');
                    string clipName = fileName.Substring(fileNameIndex);
                    string completePath = fileName.Replace("\"", "/");
                    Debug.Log($"Using Audio Clip path {completePath}");
                    Debug.Log($"Using Audio Clip name {clipName}");

                    var rvdClip = Resources.Load(completePath);
                    voiceOverFile = (AudioClip)rvdClip;
                    break;
                }
            }

        }
        catch (NullReferenceException e)
        {
            Debug.Log("file not found");
        }

        audioSrc.clip = voiceOverFile;
        audioSrc.Play();
        return voiceOverFile;
    }
}