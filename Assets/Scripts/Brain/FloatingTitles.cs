using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FloatingTitles : MonoBehaviour
{
    private Camera camera;
    public GameObject poiPosition;
    private LineRenderer lineRender;
    public Material lineMaterial;

    void Start()
    {
        camera = Camera.main;
        lineRender = GetComponent<LineRenderer>();
    }

    void LateUpdate()
    {
        transform.LookAt(Camera.main.transform);
        lineRender.SetPosition(0,this.transform.position);
        lineRender.SetPosition(1,poiPosition.transform.position);
    }

    public void setPOIPosition(GameObject position)
    {
        poiPosition = position;
    }

}