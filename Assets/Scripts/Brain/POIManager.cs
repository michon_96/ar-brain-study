using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class POIManager : MonoBehaviour
{
    [SerializeField] private GameObject brainObject;
    [SerializeField] private GameObject TextPrefab;
    private GameObject[] poiList;
    [SerializeField] private Color[] poiColors;
    [SerializeField] private float distanceMultiplier = 1.5f;
    void Start()
    {
        // if (brainObject != null)
        // {
        poiList = GameObject.FindGameObjectsWithTag("POI");
        GameObject poiParent = Instantiate(new GameObject("Buttons"),brainObject.transform);

        poiParent.transform.parent = GameObject.FindGameObjectWithTag("DisplayObject").transform;
        int partNumber = 1;

        foreach (GameObject poi in poiList)
        {
            Vector3 newPosition = brainObject.transform.position + (poi.transform.position - brainObject.transform.position) * distanceMultiplier;
            GameObject newFloatingText = Instantiate(TextPrefab, newPosition, brainObject.transform.rotation, poiParent.transform);
   
            //todo:return later
            //string partName = $"{partNumber}.{poi.name}";
            string partName = $"{partNumber}";
            newFloatingText.gameObject.name = partName;
            int colorIndex = partNumber - 1;
            partNumber++;

            newFloatingText.GetComponentInChildren<Text>().text = partName;
            newFloatingText.GetComponentInChildren<Image>().color = poiColors[colorIndex];
            newFloatingText.GetComponentInChildren<FloatingTitles>().setPOIPosition(poi);
        }
    }
}