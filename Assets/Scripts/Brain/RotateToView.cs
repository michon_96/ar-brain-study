using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToView : MonoBehaviour
{
    [SerializeField] public Vector3 targetAngle = new Vector3(0f, 345f, 0f);
    [SerializeField] private Vector3 currentAngle;

    [SerializeField] private bool isRotating;
    [SerializeField] private float rotateSpeed;
    public bool activateRotation = false;

    void Start()
    {
        currentAngle = transform.eulerAngles;
        targetAngle = -currentAngle;

        this.transform.Rotate(0,0,0);
    }

    void FixedUpdate()
    {
        if(activateRotation)
            this.transform.Rotate(new Vector3(0, Time.deltaTime * rotateSpeed, 0));
    }

    //public IEnumerator rotateTo()
    //{
    //    if (isRotating)
    //    {
    //        currentAngle = new Vector3(
    //            Mathf.LerpAngle(currentAngle.x, targetAngle.x, Time.deltaTime),
    //            Mathf.LerpAngle(currentAngle.y, targetAngle.y, Time.deltaTime),
    //            Mathf.LerpAngle(currentAngle.z, targetAngle.z, Time.deltaTime));

    //        transform.eulerAngles = currentAngle;
    //    }
    //    yield return new WaitForSeconds(0);
    //}
    /*
        public void getPOIVectorList(GameObject[] poiList)
        {
            foreach (var poi in poiList)
            {
                angles.Add(poi.gameObject.transform.position);
            }
        }*/
}
