using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BrainPart
{
   public string name;
   public string description;
   public string didYouKnow;
}
