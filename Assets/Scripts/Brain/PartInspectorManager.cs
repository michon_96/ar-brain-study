using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Video;

public class PartInspectorManager : MonoBehaviour
{
    private static PartInspectorManager instance;

    public static PartInspectorManager getInstance()
    {
        return instance;
    }

    [Header("UI Objects")]
    [SerializeField] Text title;
    [SerializeField] Text description;
    [SerializeField] Text didYouKnow;

    [SerializeField] GameObject magnifyBttn;
    [SerializeField] GameObject microscopeBttn;

    [Header("Part Info")]
    [SerializeField] private List<BrainPart> brainPartList;
    [SerializeField] private string fileName;
    [SerializeField] private string selectedPart;

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
        }

        if (instance == null)
            instance = this;

        DontDestroyOnLoad(this.gameObject);
    }

    public string getSelectedPart()
    {
        return selectedPart;
    }

    public void updateUI(int index)
    {
        //string brainPartName = brainPartList[index].name;
        ShowMagnified(index);
        selectedPart = brainPartList[index].name;
        title.text = selectedPart;
        description.text = brainPartList[index].description;
        didYouKnow.text = brainPartList[index].didYouKnow;
    }

    void ShowMagnified(int index)
    {
        if (index % 2 == 0)
        {
            magnifyBttn.SetActive(true);
            microscopeBttn.SetActive(false);
        }
        else
        {
            magnifyBttn.SetActive(false);
            microscopeBttn.SetActive(true);
        }
    }

    private void saveDescription(StreamReader reader)
    {
        string descriptionText = "";
        do
        {
            descriptionText = $"{descriptionText} \n {reader.ReadLine()}";
        } while (reader.ReadLine() != null || !reader.ReadLine().Equals(""));
    }

}
