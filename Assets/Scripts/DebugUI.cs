using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class DebugUI : MonoBehaviour
{
    private static DebugUI instance;
    [SerializeField] GameObject debugUIParent;
    [SerializeField] Text debugTextUI;
    [SerializeField] bool enableDebug = false;
    public DebugUI getInstance()
    {
        return instance;
    }

    void Start()
    {
        if (instance == null)
            instance = this;

        debugUIParent.gameObject.SetActive(enableDebug);
        addText("Debug created");
    }
    public void addText(string message)
    {
        debugTextUI.text = $"{debugTextUI.text}\n {message}";
    }
}
