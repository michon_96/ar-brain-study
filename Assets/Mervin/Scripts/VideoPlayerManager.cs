using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;

public class VideoPlayerManager : MonoBehaviour
{
    [SerializeField] MediaPlayer m_player;
    [SerializeField] AudioSource m_audio;

    [SerializeField] AudioClip m_magnifyAudioClip;
    [SerializeField] AudioClip m_microAudioClip;

    public void PlayMagnifyVideo()
    {
        m_audio.clip = m_magnifyAudioClip;
        //m_player.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToDataFolder, "Resources/Videos/magnifying glass video.mp4", true);
        m_player.OpenMedia(MediaPathType.RelativeToStreamingAssetsFolder, "Videos/magnifying glass video.mp4", true);
    }

    public void PlayMicroscopeVideo()
    {
        m_audio.clip = m_microAudioClip;
        m_player.OpenMedia(MediaPathType.RelativeToStreamingAssetsFolder, "Videos/microscope video.mp4", true);
    }

}
