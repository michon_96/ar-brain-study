﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
using UnityEngine.SceneManagement;

using System.Globalization;
using System.Text.RegularExpressions;
using Firebase;

public class AuthenticationScreen : MonoBehaviour
{
    [SerializeField] GameObject m_loginScreen;
    [SerializeField] GameObject m_registrationScreen;
    [SerializeField] GameObject m_resetPasswordScreen;
    //login
    [SerializeField] InputField m_loginEmail;
    [SerializeField] InputField m_loginPassword;
    //registration
    [SerializeField] InputField m_regEmail;
    [SerializeField] InputField m_regPassword;
    [SerializeField] InputField m_regPasswordCheck;
    //reset password
    [SerializeField] InputField m_resetPassEmail;

    //toast
    [SerializeField] GameObject m_toast;

    void Start()
    {
        ClearAllInputs();
        m_loginEmail.text = PlayerPrefs.GetString("arb_email", "");
        m_loginPassword.text = PlayerPrefs.GetString("arb_password", "");
    }

    public void Login()
    {
        FirebaseManager.instance.Login(m_loginEmail.text, m_loginPassword.text, OnLoginResponse);
    }

    void OnLoginResponse(object p_resp)
    {
        switch (p_resp)
        {
            case string respMsge:
                m_toast.SetActive(true);
                m_toast.GetComponentInChildren<Text>().text = respMsge;
                m_toast.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
                break;
            case Firebase.Auth.FirebaseUser user:
                //Debug.Log("OnResponse: LOGIN SUCCESSFUL");
                PlayerPrefs.SetString("arb_email", m_loginEmail.text);
                PlayerPrefs.SetString("arb_password", m_loginPassword.text);
                StartCoroutine("LoadYourAsyncScene");
                break;
        }
    }

    public void LoadScene()
    {
        SceneManager.LoadSceneAsync("Intro");
    }

    IEnumerator LoadYourAsyncScene()
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("Intro");

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
    }


    public void Register()
    {
        if (!IsValidEmail(m_regEmail.text))
        {
            OnRegisterResponse("INVALID EMAIL");
        }
        else if (m_regPassword.text != m_regPasswordCheck.text)
        {
            OnRegisterResponse("PASSWORD DOES NOT MATCH!");
        }
        else
        {
            FirebaseManager.instance.RegisterNewAccount(m_regEmail.text, m_regPassword.text, OnRegisterResponse);
        }
    }

    void ClearAllInputs()
    {
        m_loginEmail.text = "";
        m_loginPassword.text = "";
        m_regEmail.text = "";
        m_regPassword.text = "";
        m_regPasswordCheck.text = "";
        m_resetPassEmail.text = "";
    }

    void OnRegisterResponse(object p_resp)
    {
        switch (p_resp)
        {
            case string respMsge:
                m_toast.SetActive(true);
                m_toast.GetComponentInChildren<Text>().text = respMsge;
                m_toast.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
                break;
            case Firebase.Auth.FirebaseUser user:
                m_toast.SetActive(true);
                m_toast.GetComponentInChildren<Text>().text = "Registration Successful!";
                m_toast.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
                m_toast.GetComponentInChildren<Button>().onClick.AddListener(delegate
                {
                    ClearAllInputs();
                    m_registrationScreen.SetActive(false);
                    m_loginScreen.SetActive(true);
                });
                break;
        }
    }

    public void ResetPassword()
    {
        if (!IsValidEmail(m_resetPassEmail.text))
        {
            Debug.Log("INVALID EMAIL");
            OnResetPasswordResponse("INVALID EMAIL");
        }
        else
        {
            FirebaseManager.instance.PasswordReset(m_resetPassEmail.text, OnResetPasswordResponse);
        }
    }

    public void OnResetPasswordResponse(object p_resp)
    {
        switch (p_resp)
        {
            case string respMsge:
                m_toast.SetActive(true);
                m_toast.GetComponentInChildren<Text>().text = respMsge;
                m_toast.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
                break;
            default:
                m_toast.SetActive(true);
                m_toast.GetComponentInChildren<Text>().text = "Reset password is sent to your email!";
                m_toast.GetComponentInChildren<Button>().onClick.RemoveAllListeners();
                m_toast.GetComponentInChildren<Button>().onClick.AddListener(delegate
                {
                    ClearAllInputs();

                    m_loginScreen.SetActive(true);
                    m_resetPasswordScreen.SetActive(false);
                });
                break;
        }
    }

    public bool IsValidEmail(string email)
    {
        if (string.IsNullOrWhiteSpace(email))
            return false;

        try
        {
            // Normalize the domain
            email = Regex.Replace(email, @"(@)(.+)$", DomainMapper, RegexOptions.None, TimeSpan.FromMilliseconds(200));

            // Examines the domain part of the email and normalizes it.
            string DomainMapper(Match match)
            {
                // Use IdnMapping class to convert Unicode domain names.
                var idn = new IdnMapping();

                // Pull out and process domain name (throws ArgumentException on invalid)
                string domainName = idn.GetAscii(match.Groups[2].Value);

                return match.Groups[1].Value + domainName;
            }
        }
        catch (RegexMatchTimeoutException e)
        {
            return false;
        }
        catch (ArgumentException e)
        {
            return false;
        }

        try
        {
            return Regex.IsMatch(email,
                @"^[^@\s]+@[^@\s]+\.[^@\s]+$",
                RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
        }
        catch (RegexMatchTimeoutException)
        {
            return false;
        }
    }

}
