﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Extensions;
//using Firebase.Firestore;
using System;

public class FirebaseManager : MonoBehaviour
{
    public static FirebaseManager instance;

    public static string MSGE_INVALID_EMAIL = "INVALID EMAIL";
    public static string MSGE_LOGIN_FAIL = "LOGIN FAIL";
    public static string MSGE_PASSWORD_MISMATCH = "PASSWORD DOES NOT MATCH";
    public static string MSGE_REGISTRATION_SUCCESS = "REGISTRATION SUCCESSFUL";
    public static string MSGE_REGISTRATION_FAIL = "REGISTRATION FAIL";
    public static string MSGE_PW_RESET_FAIL = "PASSWORD RESET FAIL\nNO RECORD FOUND!";
    public static string MSGE_PW_RESET_SUCCESS = "PASSWORD RESET LINK HAS BEEN SENT TO YOUR EMAIL!";

    Firebase.Auth.FirebaseAuth auth;

    //firestore
    //FirebaseFirestore m_database;
    Firebase.Auth.FirebaseUser m_currentUser;
    Dictionary<string, object> user;

    public delegate void OnFirebaseResponse(object p_resp);
    public event OnFirebaseResponse firebaseResponse;
    object response;

    private void Awake()
    {
        instance = this;
    }
    void Start()
    {
        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        //m_database = FirebaseFirestore.DefaultInstance;
    }

    public void SaveUser()
    {
        //user = new Dictionary<string, object>
        //{
        //    {"UserID",m_currentUser.UserId },
        //    {"Email",m_currentUser.Email},
        //    {"Activity1","99"},
        //};
        //m_database.Collection("Userdata").Document(m_currentUser.UserId).SetAsync(user).ContinueWithOnMainThread(task =>
        //{
        //    if (task.IsCompleted)
        //    {
        //        Debug.Log("Save successful");
        //    }
        //    else
        //    {
        //        Debug.Log("Save failed!!!");
        //    }
        //}
        //);
    }

    public void ReadData()
    {
        //m_database.Collection("Userdata").Document(m_currentUser.UserId).GetSnapshotAsync().ContinueWithOnMainThread(task =>
        //{
        //    if (task.IsCompleted)
        //    {
        //        DocumentSnapshot snapshot = task.Result;
        //        if (snapshot.Exists)
        //        {
        //            user = snapshot.ToDictionary();
        //            foreach (KeyValuePair<string, object> pair in user)
        //            {
        //                Debug.Log(pair.Key + " : " + pair.Value);
        //            }
        //        }
        //    }
        //});
    }

    public void PasswordReset(string p_email, Action<object> OnResponse)
    {
        auth.SendPasswordResetEmailAsync(p_email).ContinueWithOnMainThread(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SendPasswordResetEmailAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SendPasswordResetEmailAsync encountered an error: " + task.Exception);
                OnResponse(MSGE_PW_RESET_FAIL);
                return;
            }
            OnResponse(null);


            Debug.Log("Password reset email sent successfully.");
        });
    }

    public void SendEmailVerification(string p_email, Action<object> OnResponse)
    {
        Firebase.Auth.FirebaseUser user = auth.CurrentUser;
        if (user != null)
        {
            user.SendEmailVerificationAsync().ContinueWithOnMainThread(task =>
            {
                if (task.IsCanceled)
                {
                    Debug.LogError("SendEmailVerificationAsync was canceled.");
                    return;
                }
                if (task.IsFaulted)
                {
                    Debug.LogError("SendEmailVerificationAsync encountered an error: " + task.Exception);
                    OnResponse("Sending email verification encountered an error Please try again");
                    return;
                }
                OnResponse(null);

                Debug.Log("Email sent successfully.");
            });
        }
    }

    public void RegisterNewAccount(string p_email, string p_password, Action<object> OnResponse)
    {
        auth.CreateUserWithEmailAndPasswordAsync(p_email, p_password).ContinueWithOnMainThread(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                OnResponse(MSGE_REGISTRATION_FAIL);
                return;
            }

            // Firebase user has been created.
            Firebase.Auth.FirebaseUser newUser = task.Result;
            OnResponse(newUser);
            Debug.LogFormat("Firebase user created successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);
        });
    }

    public void Login(string p_email, string p_password, Action<object> OnResponse)
    {
        auth.SignInWithEmailAndPasswordAsync(p_email, p_password).ContinueWithOnMainThread(task =>
        {

            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                //UnityThread.executeInUpdate(OnResponse);
                OnResponse(MSGE_LOGIN_FAIL);
                Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            m_currentUser = newUser;
            Debug.LogFormat("User signed in successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);
            OnResponse(newUser);

        });

    }

    public void LoginCredentials(string p_email, string p_password, Action<object> OnResponse)
    {
        Firebase.Auth.Credential credential = Firebase.Auth.EmailAuthProvider.GetCredential(p_email, p_password);
        auth.SignInWithCredentialAsync(credential).ContinueWithOnMainThread(task =>
        {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithCredentialAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                OnResponse(MSGE_LOGIN_FAIL + " : " + task.Exception);
                Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                return;
            }

            Firebase.Auth.FirebaseUser newUser = task.Result;
            OnResponse(newUser);
            Debug.LogFormat("User signed in successfully: {0} ({1})", newUser.DisplayName, newUser.UserId);
        });
    }
}